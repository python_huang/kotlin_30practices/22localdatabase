package com.example.a22localdatabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    lateinit var adapter: ItemsAdapter
    lateinit var dbHelper: MemberDatabaseHelper
    var items = ArrayList<ItemModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dbHelper = MemberDatabaseHelper(this)
        setupView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.layout_menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == R.id.layout_menu_plus) {
            didClickPlus()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setupView() {
        adapter = ItemsAdapter(items)
        layoutItemsRecyclerView.layoutManager =
            GridLayoutManager(this, 1)
        layoutItemsRecyclerView.adapter = adapter

        reloadData()
    }

    private fun reloadData() {
        items = dbHelper.getNames()
        adapter.items = items
        adapter.notifyDataSetChanged()
    }

    private fun didClickPlus() {
        val inputAlert = AlertDialog.Builder(this)
        inputAlert.setTitle("Add Name")
        inputAlert.setMessage("Your name is: ")

        val userInput = EditText(this)
        inputAlert.setView(userInput)
        inputAlert.setPositiveButton("新增") { _, _ ->
            addNewName("${userInput.text}")

        }

        inputAlert.setNegativeButton("取消") { _, _ ->
            println("取消")
        }

        inputAlert.show()
    }

    private fun addNewName(name: String) {
        MemberDatabaseHelper(this).addName(name)
        reloadData()
    }

}